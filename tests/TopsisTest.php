<?php

declare(strict_types=1);

namespace Topsis\Tests;

use Topsis\Topsis;
use Phpml\Math\Matrix;
use PHPUnit\Framework\TestCase;

class TopsisTest extends TestCase
{
    public function testCalculateThePowerOfGivenColumn(): void
    {
        $evaluation = [
            [1,2,3],
            [4,5,6]
        ];

        $expected = 17;

        $matrix = new Matrix($evaluation);

        $topis = new Topsis($matrix);
        $squereSum = $topis->calculateThePowerOfGivenColumn($matrix->getColumnValues(0));

        self::assertEquals($expected, $squereSum);
    }

    public function testNormalizedMatrix(): void
    {
        $evaluation = [
            [1,2,3],
            [4,5,6]
        ];

        $expected = [
            [ 0.24253562503633, 0.3713906763541, 0.44721359549996 ],
            [ 0.97014250014533, 0.92847669088526, 0.89442719099992 ]
        ];

        $matrix = new Matrix($evaluation);

        $topis = new Topsis($matrix);
        $normalized = $topis->getNormalizedMatrix();

        self::assertEquals($expected, $normalized->toArray());
    }

    public function testCalculateTheSumOfGivenColumn(): void
    {
        $evaluation = [
            [1,2,3],
            [4,5,6]
        ];

        $weightsOfCriterion = [
            5, 4, 3
        ];

        $expectedNormalized = [
            [ 0.24253562503633, 0.3713906763541, 0.44721359549996 ],
            [ 0.97014250014533, 0.92847669088526, 0.89442719099992 ]
        ];

        $expectedSumOfColumn = 1.0611398968904;

        $matrix = new Matrix($evaluation);

        $topis = new Topsis($matrix, $weightsOfCriterion);

        $normalized = $topis->getNormalizedMatrix();
        self::assertEquals($expectedNormalized, $normalized->toArray());

        $sumOfColumn = $topis->calculateTheSumOfGivenColumn(
            $normalized->transpose()->getColumnValues(0)
        );
        self::assertEquals($expectedSumOfColumn, $sumOfColumn);
    }

    public function testWeightedMatrix(): void
    {
        $evaluation = [
            [1,2,3],
            [4,5,6]
        ];

        $weightsOfCriterion = [
            5, 4, 3
        ];

        $expectedNormalized = [
            [ 0.24253562503633, 0.3713906763541, 0.44721359549996 ],
            [ 0.97014250014533, 0.92847669088526, 0.89442719099992 ]
        ];

        $expectedWeighted = [
            [ 1.1428070217088, 1.3999687597929, 1.2643392171301 ],
            [ 1.7367103288848, 1.3296974899647, 0.96070068519558 ]
        ];

        $matrix = new Matrix($evaluation);

        $topis = new Topsis($matrix, $weightsOfCriterion);

        $normalized = $topis->getNormalizedMatrix();
        self::assertEquals($expectedNormalized, $normalized->toArray());

        $weighted = $topis->getWeighted();
        self::assertEquals($expectedWeighted, $weighted->toArray());
    }

    public function testDetermineTheMaximaAndTheMinimaOfGivenMatrixByColumns(): void
    {
        $evaluation = [
            [1,2,3],
            [4,5,6]
        ];

        $expectedMaxima = [ 4,5,6 ];
        $expectedMinima = [ 1,2,3 ];

        $matrix = new Matrix($evaluation);

        $topis = new Topsis($matrix);

        self::assertEquals($expectedMaxima, $topis->determineTheMaximaOfGivenMatrixByColumns($matrix));
        self::assertEquals($expectedMinima, $topis->determineTheMinimaOfGivenMatrixByColumns($matrix));
    }
}

