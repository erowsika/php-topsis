# erowsika/php-topsis - MCDMA TOPSIS library for PHP

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.1-8892BF.svg)](https://php.net/)

erowsika/php-topsis requires PHP >= 7.1 & php-ai/php-ml >= 0.7.0

## References
- [Wiki TOPSIS](https://en.wikipedia.org/wiki/TOPSIS)
- [php-ai/php-ml Matrix](http://php-ml.readthedocs.io/en/latest/math/matrix/)
- [github.com/php-ai/php-ml](https://github.com/php-ai/php-ml)

## License

erowsika/php-topsis is released under the MIT Licence. See the bundled LICENSE file for details.
