<?php

declare(strict_types=1);

namespace Topsis;

use Phpml\Math\Matrix;

class Topsis
{
    /**
     * @var Phpml\Math\Matrix
     */
    private $matrix;

    /**
     * @var array
     */
    private $weightsOfCriterion;

    /**
     * @var Phpml\Math\Matrix
     */
    private $normalized;

    /**
     * @var Phpml\Math\Matrix
     */
    private $weighted;

    /**
     * @var array
     */
    private $distanceOverMinima;

    /**
     * @var array
     */
    private $distanceOverMaxima;

    /**
     * @var array
     */
    private $distanceOverIdealSolutions;

    /**
     * Create new a instance 
     * 
     * @param Phpml\Math\Matrix|null $dataset
     * @return void
     **/
    public function __construct(Matrix $matrix = null, array $weightsOfCriterion = [])
    {
        $this->matrix = $matrix;
        $this->weightsOfCriterion = $weightsOfCriterion;
    }

    public function getNormalizedMatrix(): Matrix
    {
        $normalized = $row = [];

        for ($i=0; $i < $this->matrix->getRows(); $i++) {
            for ($j=0; $j < $this->matrix->getColumns(); $j++) {
                $row[] = $this->matrix->getColumnValues($j)[$i] / sqrt(
                    $this->calculateThePowerOfGivenColumn(
                        $this->matrix->getColumnValues($j)
                    )
                );
            }
            $normalized[] = $row;
            unset($row);
        }

        return $this->normalized = new Matrix($normalized);
    }

    public function getWeighted(): Matrix
    {
        $weighted = $row = [];
        $transposed = $this->normalized->transpose();

        for ($i=0; $i < $this->normalized->getRows(); $i++) {
            for ($j=0; $j < $this->normalized->getColumns(); $j++) {
                $row[] = $this->normalized->getColumnValues($j)[$i] * $this->weightsOfCriterion[$j] / $this->calculateTheSumOfGivenColumn($transposed->getColumnValues($i));
            }
            $weighted[] = $row;
            unset($row);
        }

        return $this->weighted = new Matrix($weighted);
    }

    public function calculateThePowerOfGivenColumn(array $column): int
    {
        $squareSum = 0;
        foreach ($column as $key => $value) {
            $squareSum += pow($value,2);
        }

        return $squareSum;
    }

    public function calculateTheSumOfGivenColumn(array $column): float
    {
        $sum = 0;
        foreach ($column as $key => $value) {
            $sum += $value;
        }

        return $sum;
    }

    public function determineTheMaximaOfGivenMatrixByColumns(Matrix $matrix): array
    {
        $maxima = [];

        for ($i=0; $i < $matrix->getColumns(); $i++) {
            $maxima[] = max($matrix->getColumnValues($i));
        }

        return $this->distanceOverMaxima = $maxima;
    }

    public function determineTheMinimaOfGivenMatrixByColumns(Matrix $matrix): array
    {
        $minima = [];

        for ($i=0; $i < $matrix->getColumns(); $i++) {
            $minima[] = min($matrix->getColumnValues($i));
        }

        return $this->distanceOverMinima = $minima;
    }

    public function calculateTheDistanceOfWeightedAndMinima(Matrix $weighted, array $minima): array
    {
        $distances = [];
        $sum = 0;

        for ($i=0; $i < $weighted->getRows(); $i++) {
            for ($j=0; $j < $weighted->getColumns(); $j++) {
                $sum += pow($weighted->getColumnValues($j)[$i] - $minima[$j], 2);
            }
            $distances[] = sqrt($sum);
            unset($sum);
        }

        return $this->distanceOverMinima = $distances;
    }

    public function calculateTheDistanceOfWeightedAndMaxima(Matrix $weighted, array $maxima): array
    {
        $distances = [];
        $sum = 0;

        for ($i=0; $i < $weighted->getRows(); $i++) {
            for ($j=0; $j < $weighted->getColumns(); $j++) {
                $sum += pow($weighted->getColumnValues($j)[$i] - $maxima[$j], 2);
            }
            $distances[] = sqrt($sum);
            unset($sum);
        }

        return $this->distanceOverMaxima = $distances;
    }

    public function calculateTheSimilarityToTheWorstCondition(): array
    {
        $similarity = [];

        for ($i=0; $i < $this->weighted->getRows(); $i++) {
            $similarity[] = $this->distanceOverMinima[$i] / ($this->distanceOverMinima[$i] + $this->distanceOverMaxima[$i]);            
        }

        return $this->distanceOverIdealSolutions = $similarity;
    }
}
